public class SavingsAccount extends Account{
    public SavingsAccount(String name, double balance){
        super(name, balance);
    }

    public void addInterest(){
        this.setBalance(getBalance()* 10/100);
    }
}