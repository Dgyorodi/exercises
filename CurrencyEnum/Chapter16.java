package CurrencyEnum;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;


public class Chapter16 {
    public static void main(String[] args) {
        // 1
        LocalDate today = LocalDate.now();
        LocalDate myBirthday = LocalDate.of(1998, 05, 29);

        LocalDate nextBday = myBirthday.withYear(today.getYear());

        if (nextBday.isBefore(today) || nextBday.isEqual(today)){
            nextBday = nextBday.plusYears(1);
        }

        Period p = Period.between(today, nextBday);
        long p2 = ChronoUnit.DAYS.between(today, nextBday);
        System.out.println(p.getMonths() + "Months "  + "  and " + p.getDays() + " days until next birthday. (" + p2 + " total)\n");

        
        //2
        /*
        LocalDate now = LocalDate.now();
        LocalTime nowNY = LocalTime.now();
        SimpleTimeZone time = new SimpleTimeZone(400, "America/Los_Angeles");
        System.out.println(time + " " + nowNY);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"));
        Date currentDate = calendar.getTime();
        */
    }
}