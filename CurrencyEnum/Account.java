package CurrencyEnum;

public class Account {
	private String name;
	private double balance;
	private Currency currency;
	
	public String getName() {
		return name;
	}

	public void setName(String x) {
		name = x;
	}

	public double getBalance() {
		return balance;
	}
	public void setBalance(double b) {
		balance = b;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	public Account(String name, double balance, Currency currency) {
		this.name = name;
		this.balance = balance;
		this.currency = currency;
	}
	
	
	
	
	
	}
}