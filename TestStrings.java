import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TestStrings {
    public static void main(String[] args) {
        StringBuilder s = new StringBuilder("example.doc");
        System.out.println(s);
        
        s.replace(s.length()-3, s.length(), "bak");
        System.out.println(s);


        String a = "Diana";
        String b = "Ana";

        if (a.equals(b))
            System.out.println("equal\n");
        else {
            System.out.println("not equal\n");
        }
       
        if (a.compareTo(b) > 0)
            System.out.println(a);
        else
            System.out.println(a);

        
        String x = "the quick brown fox swallowed down the lazy chicken";

        int count = 0;
        for (int i = 0; i < x.length() -2 ; i++) {
            if (x.substring(i, i+2).equals("ow"))
                count++;
        }
        System.out.printf("the text ow appears %d times\n", count);
        

        StringBuilder f = new StringBuilder("Live not on evil");
        StringBuilder g = f.reverse();

        if (f.toString().equalsIgnoreCase(g.toString()))
            System.out.println("the text is a palindrome \n");

        
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        Format yearOnly = new SimpleDateFormat("yyyy");
        Format time = new SimpleDateFormat("mm hh");
        
        System.out.println(dayMonthYear.format(new Date()));
        System.out.println(yearOnly.format(new Date()));
        System.out.println(time.format(new Date()));

    }
}