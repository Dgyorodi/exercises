public class Account implements Detailable{
    private double balance;
    private String name;

    private static double interestRate=2;

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setName(String name){
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public Account(){
        
    }

    public void addInterest(){
    }

    public Account(String s, double x){
        name = s;
        balance=x;
    }
    
    public Account(String s){
        this.name = "Diana";
        this.balance = 50;
    }

    public static void setInterest(double x) {
        interestRate = x;
    }

    public static double getInterest(){
        return interestRate;
    }

    public boolean withdraw(double amount){
        if (balance>amount){
            balance -= amount;
            System.out.println("You have withdrawn " + amount);
            return true;
        }
        else{
            System.out.println("Not enough money in the account");
            return false;
        }    
    }

    public boolean withdraw(){
        return withdraw(20);
    }

    @Override
    public String getDetails() {
    		return "Name: " + name + " Balance: " + balance;

    }

}