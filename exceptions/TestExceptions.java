package exceptions;

public class TestExceptions {
    	public static void main(String[] args) {
		
		Account[] accounts;
		accounts = new Account[5];

		double[] amounts = {12,23,400,300,350};
		String[] names = {"Diana", "Anna", "Francis", "Ana", "Fin"};

		
		try{
			for (int i=0; i<accounts.length; i++){
				accounts[i] = new Account();
				accounts[i].setName(names[i]);
				accounts[i].setBalance(amounts[i]);

				System.out.println("Name: " + accounts[i].getName());
				System.out.println("Balance " + accounts[i].getBalance()); 

				accounts[i].addInterest();
				System.out.println("New balance: " + accounts[i].getBalance() + "\n");
			}
		}
		catch (DodgyNameException e){
			System.out.println("Exception: " + e);
			return;
		}
		finally{
			double tax = 0;

			for (Account account : accounts) {
				tax += account.getBalance() * 0.4;
			}

			System.out.println("Tax collected: "+ tax);
		}
    }
}