import java.util.HashSet;
import java.util.*;

import javax.swing.text.html.HTMLDocument.Iterator;

public class CollectionsTest {
    
    public static void main(String[] args) {
        HashSet<Account> accounts;
        accounts = new HashSet<Account>();

        String [] names ={"Diana", "Anna", "Francis"};
        double [] balances = {100, 200, 300};

        
        Account b;
        for (int i = 0; i < balances.length; i++) {
            b = new Account();
            b.setName(names[i]);
            b.setBalance(balances[i]);
            accounts.add(b);
        }
        
        Iterator<Account> iterate = accounts.iterator();
    
        while (iterate.hasNext())
		{
			Account a = iterate.next();
			System.out.println("The name is " + a.getName());
			System.out.println("The balance is " + a.getBalance());
			a.addInterest();
			System.out.println("The new balance is " + a.getBalance() + "\n");
		}


        for (Account x : accounts) {
            System.out.println("Name:" + x.getName());
            System.out.println("Balance:" +x.getBalance());
            
			x.addInterest();
			System.out.println("Updated balance: " + x.getBalance() + "\n");
        }
    }  
}