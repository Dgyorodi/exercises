public class CurrentAccount extends Account{
    public CurrentAccount(String name, double balance){
        super(name, balance);
    }

    public void addInterest(){
        this.setBalance(getBalance()* 1.6);
    }
}