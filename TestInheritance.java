public class TestInheritance {
    public static void main(final String[] args) {
        Account[] accounts = { new SavingsAccount("One", 2),
                                new SavingsAccount("Anna", 4),
                                new CurrentAccount("Diana", 6),
        };

        for (int i=0; i<accounts.length; i++){
			accounts[i].addInterest();
			System.out.println("Account name: " + accounts[i].getName());
            System.out.println("Balance " + accounts[i].getBalance());
            
		}

    }
}