public class MyFirstClass {
    public static void main(String[] args) {
        String make = "BMW";
        String model = "100A";
        double engineSize = 2.0;
        byte gear = 3;
        short speed = (short)(gear*20);

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is "+ engineSize);
        System.out.println(speed);

        if (engineSize > 1.5){
            System.out.println("Powerful car\n");
        }else{
            System.out.println("Weak car\n");
        }

        if(gear < 2){
            System.out.println("Speed less than 10mph");
        }else if(gear == 2){
            System.out.println("Speed 10mph");
        }else{
            System.out.println("Speed more than 10mph");
        }

        int ok = 0;
        for(int i = 1970; i < 2001; i += 4){
            ok++;
            System.out.println(i);
            if(ok == 5)
                break;
        }
        
        // ch 4 - 3
        switch (gear) {
            case 1:
                System.out.println("Speed less than 10mph\n");
            case 2:
                System.out.println("Speed 10mph\n");
            case 3:
                System.out.println("Speed more than 10mph\n");
                break;
            default:
                break;
        }
        
        //ch 4 - 4
        int a[] = new int[100];
        int x = 0;
        for(int i = 1960; i < 2001; i += 4){
            a[x++] = i;
        }
        for (int j = 0; j < 10; j++) {
            System.out.println(a[j]);
        }


        
    }
}