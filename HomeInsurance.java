public class HomeInsurance implements Detailable{
    double premium;
    double excess; 
    double insured;

    public HomeInsurance(double premium, double excess, double insured) {
        this.premium = premium;
        this.excess = excess;
        this.insured = insured;
    }

    @Override
    public String getDetails() {
        return  "Premium: " + premium + " Excess: " + excess;
    }
}